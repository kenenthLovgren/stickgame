﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stickGame.Players
{
    internal class HumanPlayers
    {
        public string name { get; set; }

        private int _wins;

        private int win
        {
           get { return _wins; }
        }

        private int _losses
        {
            get { return _losses; }
        }

        public int MakeMove(int SticksRemaining)
        {
                int picked = -1;
                string pickedStr = "";
                do
                {
                    Console.Write("{0} pick between 1 and {1} sticks ", playerName,
                        Math.Min(3, SticksRemaining));
                    pickedStr = Console.ReadLine();
                } while (!Int32.TryParse(pickedStr, out picked) || picked < 1 ||
                         picked > Math.Min(3, SticksRemaining));

                return picked;

        }

        public void GameOver(bool YouWon)
        {
            if (YouWon)
            {
                _wins++;
            }else{
                _losses++;
            }
        }
        }
    }