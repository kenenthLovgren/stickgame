﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame
{

    
    class Program
    {

        //Intialize Players
        static string[] InitializePlayers(int noOfplayers)
        {
            string[] playerNames = new string[noOfplayers];

            for (int i = 0; i < playerNames.Length; i++)
            {
                Console.Write("Player {0} type your name: ", i + 1);
                playerNames[i] = Console.ReadLine();
            }
            return playerNames;
        }

        // Ask for players move
        static int MakeMove(int sticksRemaining, string playerName)
        {
            int picked = -1;
            string pickedStr = "";
            do
            {
                Console.Write("{0} pick between 1 and {1} sticks ", playerName,
                    Math.Min(3, sticksRemaining));
                pickedStr = Console.ReadLine();
            } while (!Int32.TryParse(pickedStr, out picked) || picked < 1 ||
                     picked > Math.Min(3, sticksRemaining));

            return picked;
        }


        //Print game statis
        static void PrintGameStatus(int sticksRemaining)
        {
            Console.Write("{0} Sticks remaining on table", sticksRemaining);
            for (int i = 0; i < sticksRemaining; i++)
            {
                Console.Write("|");
            }
            Console.WriteLine();
        }

        //Ask to play again?
        static bool PlayAgain(string winnerName)
        {
            Console.WriteLine("{0} won the game!", winnerName);
            Console.WriteLine("Player again? (y/n)");
            return (Console.ReadKey().Key == ConsoleKey.Y);
        }


        // Main
        static void Main(string[] args)
        {
            string[] playerNames = InitializePlayers(2);
            int inTurn = 0;

            do
            {
                int sticksRemaining = 15;
                inTurn = 0;

                while (sticksRemaining > 0)
                {
                    PrintGameStatus(sticksRemaining);

                    int picked = MakeMove(sticksRemaining, playerNames[inTurn]);

                    sticksRemaining -= picked;

                    inTurn = ++inTurn % 2;
                }

            } while (PlayAgain(playerNames[inTurn]));
        }
    }
}
