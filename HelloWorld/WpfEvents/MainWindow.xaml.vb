﻿Class MainWindow
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AddHandler myBtn.Click, AddressOf Me.myBtn_Click
    End Sub

    Private Sub myBtn_Click(sender As Object, e As RoutedEventArgs)
        myLbl.Content = "Hello world"
    End Sub
End Class
