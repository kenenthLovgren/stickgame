﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.txtFullName = New System.Windows.Forms.TextBox()
        Me.FirstNamelbl = New System.Windows.Forms.Label()
        Me.LastNamelbl = New System.Windows.Forms.Label()
        Me.lblFullName = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(91, 41)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(157, 20)
        Me.txtFirstName.TabIndex = 1
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(59, 186)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(157, 23)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "String Test"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(91, 84)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(157, 20)
        Me.txtLastName.TabIndex = 3
        '
        'txtFullName
        '
        Me.txtFullName.Location = New System.Drawing.Point(91, 125)
        Me.txtFullName.Name = "txtFullName"
        Me.txtFullName.Size = New System.Drawing.Size(157, 20)
        Me.txtFullName.TabIndex = 4
        '
        'FirstNamelbl
        '
        Me.FirstNamelbl.AutoSize = True
        Me.FirstNamelbl.Location = New System.Drawing.Point(12, 44)
        Me.FirstNamelbl.Name = "FirstNamelbl"
        Me.FirstNamelbl.Size = New System.Drawing.Size(57, 13)
        Me.FirstNamelbl.TabIndex = 5
        Me.FirstNamelbl.Text = "First Name"
        '
        'LastNamelbl
        '
        Me.LastNamelbl.AutoSize = True
        Me.LastNamelbl.Location = New System.Drawing.Point(12, 87)
        Me.LastNamelbl.Name = "LastNamelbl"
        Me.LastNamelbl.Size = New System.Drawing.Size(58, 13)
        Me.LastNamelbl.TabIndex = 6
        Me.LastNamelbl.Text = "Last Name"
        '
        'lblFullName
        '
        Me.lblFullName.AutoSize = True
        Me.lblFullName.Location = New System.Drawing.Point(12, 132)
        Me.lblFullName.Name = "lblFullName"
        Me.lblFullName.Size = New System.Drawing.Size(54, 13)
        Me.lblFullName.TabIndex = 7
        Me.lblFullName.Text = "Full Name"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(278, 232)
        Me.Controls.Add(Me.lblFullName)
        Me.Controls.Add(Me.LastNamelbl)
        Me.Controls.Add(Me.FirstNamelbl)
        Me.Controls.Add(Me.txtFullName)
        Me.Controls.Add(Me.txtLastName)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.txtFirstName)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtFirstName As TextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents txtLastName As TextBox
    Friend WithEvents txtFullName As TextBox
    Friend WithEvents FirstNamelbl As Label
    Friend WithEvents LastNamelbl As Label
    Friend WithEvents lblFullName As Label
End Class
