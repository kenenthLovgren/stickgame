﻿Imports System.IO

Module Module1

    Sub Main()

        Try

            Dim myReader As StreamReader = New StreamReader("nemAtFinde.txt")

            Dim line As String = ""

            While Not IsNothing(line)
                line = myReader.ReadLine()
                If Not IsNothing(line) Then
                    Console.WriteLine(line)
                End If
            End While

            myReader.Close()


        Catch ex As DirectoryNotFoundException
            Console.WriteLine("The specified directory does not exists")

        Catch ex As FileNotFoundException
            Console.WriteLine("The specified file does not exists")

        Catch ex As Exception

            Console.WriteLine("Something vent wrong.!: {0}", ex.Message)

        Finally
            'code that should execute no matter what.
            'closing database connections
            'closing file connection
            'clean up work

        End Try




        Console.ReadLine()

    End Sub

End Module
