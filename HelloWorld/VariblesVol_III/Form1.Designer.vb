﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVariables
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtVaribles = New System.Windows.Forms.TextBox()
        Me.lblTransfer = New System.Windows.Forms.Label()
        Me.btnTransfer = New System.Windows.Forms.Button()
        Me.cmdTransferToLabel = New System.Windows.Forms.Button()
        Me.btnTranfer = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtVaribles
        '
        Me.txtVaribles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVaribles.Location = New System.Drawing.Point(95, 48)
        Me.txtVaribles.Name = "txtVaribles"
        Me.txtVaribles.Size = New System.Drawing.Size(174, 22)
        Me.txtVaribles.TabIndex = 0
        '
        'lblTransfer
        '
        Me.lblTransfer.AutoSize = True
        Me.lblTransfer.Location = New System.Drawing.Point(12, 53)
        Me.lblTransfer.Name = "lblTransfer"
        Me.lblTransfer.Size = New System.Drawing.Size(72, 13)
        Me.lblTransfer.TabIndex = 1
        Me.lblTransfer.Text = "Label Caption"
        '
        'btnTransfer
        '
        Me.btnTransfer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTransfer.Location = New System.Drawing.Point(95, 105)
        Me.btnTransfer.Name = "btnTransfer"
        Me.btnTransfer.Size = New System.Drawing.Size(152, 23)
        Me.btnTransfer.TabIndex = 2
        Me.btnTransfer.Text = "Transfer"
        Me.btnTransfer.UseVisualStyleBackColor = True
        '
        'cmdTransferToLabel
        '
        Me.cmdTransferToLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdTransferToLabel.Location = New System.Drawing.Point(95, 76)
        Me.cmdTransferToLabel.Name = "cmdTransferToLabel"
        Me.cmdTransferToLabel.Size = New System.Drawing.Size(152, 23)
        Me.cmdTransferToLabel.TabIndex = 3
        Me.cmdTransferToLabel.Text = "Transfer To Label"
        Me.cmdTransferToLabel.UseVisualStyleBackColor = True
        '
        'btnTranfer
        '
        Me.btnTranfer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTranfer.Location = New System.Drawing.Point(95, 134)
        Me.btnTranfer.Name = "btnTranfer"
        Me.btnTranfer.Size = New System.Drawing.Size(152, 23)
        Me.btnTranfer.TabIndex = 4
        Me.btnTranfer.Text = "Transfer txt to btn"
        Me.btnTranfer.UseVisualStyleBackColor = True
        '
        'frmVariables
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(307, 199)
        Me.Controls.Add(Me.btnTranfer)
        Me.Controls.Add(Me.cmdTransferToLabel)
        Me.Controls.Add(Me.btnTransfer)
        Me.Controls.Add(Me.lblTransfer)
        Me.Controls.Add(Me.txtVaribles)
        Me.Name = "frmVariables"
        Me.Text = "Transferring information"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtVaribles As TextBox
    Friend WithEvents lblTransfer As Label
    Friend WithEvents btnTransfer As Button
    Friend WithEvents cmdTransferToLabel As Button
    Friend WithEvents btnTranfer As Button
End Class
