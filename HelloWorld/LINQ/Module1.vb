﻿Module Module1

    Sub Main()

        Dim myCars As New List(Of Car) From {
            New Car With {.Make = "BMW", .Model = "550i", .Color = CarColor.black, .StickerPrice = 55000, .Year = 2009},
            New Car With {.Make = "Toyota", .Model = "4Runner", .Color = CarColor.Red, .StickerPrice = 35000, .Year = 2010},
            New Car With {.Make = "BMW", .Model = "745li", .Color = CarColor.white, .StickerPrice = 75000, .Year = 2008},
            New Car With {.Make = "Ford", .Model = "Esacpe", .Color = CarColor.white, .StickerPrice = 28000, .Year = 2008},
            New Car With {.Make = "BMW", .Model = "550i", .Color = CarColor.Red, .StickerPrice = 57000, .Year = 2010}
        }

        'Dim newCars = From myCar In myCars
        '              Where myCar.Year > 2009
        '              Select New With {myCar.Make, myCar.Model, myCar.Year}

        'Dim Bmws = From car In myCars
        '           Where car.Make = "BMW" And car.Year = 2010
        '           Select car

        'For Each myCar In myCars
        '    Console.WriteLine("{0} {1} {2}", myCar.Make, myCar.Model, myCar.Year)
        'Next

        'Dim _bmws = myCars.Where(Function(car) car.Year = 2010).Where(Function(car) car.Make = "BMW")

        'Dim _orderedCars = myCars.OrderByDescending(Function(car) car.Year)

        Dim sum = myCars.Sum(Function(car) car.StickerPrice)

        For Each myCar In _orderedCars
            Console.WriteLine("{0} {1} {2} {3} {4}", myCar.Make, myCar.Model, myCar.Color, myCar.StickerPrice, myCar.Year)
        Next

        Console.Readline()

    End Sub

End Module
