﻿Module Module1

    Sub Main()

        'Console.WriteLine("Type in a super hero's name to see this nickname")

        'Dim userValue As String = Console.ReadLine()

        'Select Case userValue.ToUpper()
        '    Case "BATMAN"
        '        Console.WriteLine("Caped Crusader")
        '    Case "SUPERMAN"
        '        Console.WriteLine("Man of Steel")
        '    Case "Green LANTERN"
        '        Console.WriteLine("Emerald Knight")
        '    Case Else
        '        Console.WriteLine("Does not compute")
        'End Select

        'Console.ReadLine()

        Console.WriteLine("Type in a super hero's name to see his nickname:")

        Dim userValue As String = Console.ReadLine()

        Dim myValue As Superhero

        If Superhero.TryParse(userValue, True, myValue) Then

            Select Case myValue
                Case Superhero.Batman
                    Console.WriteLine("Caped crusader")
                Case Superhero.Superman
                    Console.WriteLine("Man of steel")
                Case Superhero.Greenlantern
                    Console.WriteLine("Emerald Knight")
                Case Else
                    Console.WriteLine("does not compute")
            End Select
        Else
            Console.WriteLine("Unable to find a super hero by that name")
        End If

        Console.ReadLine()
    End Sub

End Module
