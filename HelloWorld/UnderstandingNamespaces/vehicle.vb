﻿Public MustInherit Class vehicle

    Public Property make As String
    Public Property Model As String
    Public Property Year As Integer
    Public Property Color As String

    Public MustOverride Function FormatMe() As String


End Class
