﻿Module Module1

    Sub Main()
        Dim myCar As New Car()
        myCar.make = "ford"
        myCar.Model = "Mondeo"
        myCar.Year = 1996
        myCar.Color = "Metallic Blue"

        Dim myTruck As New Truck()
        With myTruck
            .make = "Ford"
            .Model = "F950"
            .Year = 2005
            .Color = "Red"
            .TowingCapacity = 1200
        End With

        PrintVehiclesDetails(myCar)
        PrintVehiclesDetails(myTruck)

        Console.ReadLine()
    End Sub

    Sub PrintVehiclesDetails(ByVal _car As vehicle)
        Console.WriteLine("Here are the Vehicle's Details {0}", _car.FormatMe)

    End Sub

End Module
