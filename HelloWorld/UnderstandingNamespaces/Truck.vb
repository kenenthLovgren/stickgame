﻿Public NotInheritable Class Truck
    Inherits vehicle

    Public Property TowingCapacity As Integer

    Public Overrides Function FormatMe() As String
        Return String.Format("{0} - {1} - {2} - {3}",
                             Me.Color,
                             Me.TowingCapacity,
                             Me.Year)
    End Function
End Class
