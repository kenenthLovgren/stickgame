﻿Module Module1

    Sub Main()
        Dim myValue As Date = Now()

        Console.WriteLine(myValue.ToShortDateString())
        Console.WriteLine(myValue.ToShortTimeString())
        Console.WriteLine(myValue.ToLongDateString())
        Console.WriteLine(myValue.AddDays(3).ToShortDateString())
        Console.WriteLine(myValue.AddHours(3).ToShortTimeString())
        Console.WriteLine(myValue.AddDays(-3).ToShortDateString())
        Console.WriteLine(myValue.Month())
        Console.WriteLine(myValue.DayOfWeek())

        'Dim myBirthdate As New Date(1987, 4, 27)
        Dim myBirthdate As New Date
        myBirthdate = Date.Parse("7/01/2015")
        Dim myAge As TimeSpan = Date.Now.Subtract(myBirthdate)
        Console.WriteLine(myBirthdate)

        Console.WriteLine(myAge.TotalDays)

        Console.ReadLine()
    End Sub

End Module
